#ifndef SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_BUFFER_H_
#define SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_BUFFER_H_

#include "matrix.h"
#include "reserve_politics.h"
#include "type_traits.h"

namespace meta {
	
	struct capacity {
		dimension used, max;
		capacity();
		capacity(
			dimension const& u,
			dimension const& m);
		void discard();
	};

	template<
		typename ValType,
		typename SizeType = size_t,
		typename ReservePolitics = double_size>
	class buffer : basic_traits<ValType, SizeType> {
	public:

		typedef ReservePolitics reserve_politics;

		buffer();
		buffer(dimension const& dim);
		buffer(
			dimension  const& dim,
			value_type const& init_val);
		buffer(buffer const& that);
		buffer(buffer&& that);
		~buffer();

		buffer& operator= (buffer const& that);
		buffer& operator= (buffer&& that);

		inline pointer* get() const;
		inline pointer* get();

		inline dimension dim() const;

		void clear();

		void set_value(
			value_type const& val);

		void set_value(
			range const& rows,
			range const& cols,
			value_type const& val);


		buffer clone() const;

		buffer reserve(
			dimension const& new_dim) const;

		buffer reserve(
			dimension const&  new_dim,
			value_type const& init_val) const;

		buffer insert_rows(
			size_type         position,
			size_type         count,
			value_type const& init_val) const;

		buffer insert_cols(
			size_type		  position,
			size_type		  count,
			value_type const& init_val) const;

		buffer erase_rows(range const& rows) const;
		buffer erase_cols(range const& cols) const;

	private:
		
		dimension reserved(dimension const& used) const {
			return dimension(_ResPol(used.first), _ResPol(used.second));
		}

		bool is_greater(
			dimension const& lft,
			dimension const& rht) const
		{
			bool both_equal   = lft == rht,
				 both_greater = lft >= rht;
			return
				both_greater && !both_equal;
		}

		void discard();

		void reset(
			capacity const& cap,
			pointer* ptr);

		buffer(
			capacity const& cap,
			pointer* ptr);

		capacity		  _Cap;
		pointer*		  _Ptr;
		reserve_politics  _ResPol;
	};


#define BUFFHEADER \
	template<\
	typename ValType, \
	typename SizeType = size_t, \
	typename ReservePolitics = double_size>\

#define BUFFINSTANCE buffer<ValType, SizeType, ReservePolitics>
#define BUFFNAMESPACE BUFFINSTANCE::

	BUFFHEADER BUFFNAMESPACE
		buffer(
		capacity const& cap,
		pointer* ptr) :

		_Cap(cap), _Ptr(ptr), _ResPol() {}

	BUFFHEADER void BUFFNAMESPACE
		reset(
			capacity const& cap,
			pointer* ptr) {
				_Cap = cap;
				_Ptr = ptr;
			}

	BUFFHEADER void BUFFNAMESPACE
		discard() {
			_Cap.discard();
			_Ptr = nullptr;
		}

	BUFFHEADER void BUFFNAMESPACE
		clear() {
			delete_matrix(_Ptr, _Cap.max);
			discard();
		}

	BUFFHEADER void BUFFNAMESPACE
		set_value(
			range const& rows,
			range const& cols,
			value_type const& val)
	{
		init_matrix(_Ptr, rows, cols, val);
	}

	BUFFHEADER void BUFFNAMESPACE
		set_value(value_type const& val)
	{
		range
			rows(0, _Cap.used.first),
			cols(0, _Cap.used.second);
		set_value(rows, cols, val);
	}

	BUFFHEADER BUFFNAMESPACE
		buffer() : _Cap(), _Ptr(nullptr), _ResPol() {}

	BUFFHEADER BUFFNAMESPACE
		buffer(dimension const& dim) :
		_Cap(), _Ptr(nullptr), _ResPol()
	{
		dimension max = reserved(dim);
		capacity  cap = capacity(dim, max);
		pointer*  ptr = nullptr;
		if (max.first) {
			ptr = create_matrix<value_type>(max);
		}
		reset(cap, ptr);
	}

	BUFFHEADER BUFFNAMESPACE
		buffer(
			dimension  const& dim,
			value_type const& init_val) :
		_Cap(), _Ptr(nullptr), _ResPol()
	{
		BUFFINSTANCE buf(dim);
		buf.set_value(init_val);
		*this = std::move(buf);
	}

	BUFFHEADER BUFFNAMESPACE
		buffer(BUFFINSTANCE const& that) :
		_Cap(that._Cap),
		_Ptr(nullptr),
		_ResPol()
	{
		_Ptr = create_matrix<value_type>(_Cap.max);
		copy_matrix(that._Ptr, _Cap.used, _Ptr);
	}

	BUFFHEADER BUFFNAMESPACE
		buffer(BUFFINSTANCE&& that) :
		_Cap(that._Cap),
		_Ptr(that._Ptr),
		_ResPol()
	{
		that.discard();
	}

	BUFFHEADER BUFFNAMESPACE
		~buffer() {
			clear();
		}

	BUFFHEADER BUFFINSTANCE& BUFFNAMESPACE
		operator= (BUFFINSTANCE const& that) {
			pointer*
				ptr = create_matrix<value_type>(that._Cap.max);
			copy_matrix(that._Ptr, that._Cap.used, ptr);
			clear();
			reset(that._Cap, ptr);
			return *this;
		}

	BUFFHEADER BUFFINSTANCE& BUFFNAMESPACE
		operator= (BUFFINSTANCE&& that) {
			clear();
			reset(that._Cap, that._Ptr);
			that.discard();
			return *this;
		}

	BUFFHEADER ValType** BUFFNAMESPACE
		get() const {
			return _Ptr;
		}
	BUFFHEADER ValType** BUFFNAMESPACE
		get() {
			return _Ptr;
		}

	BUFFHEADER dimension BUFFNAMESPACE
		dim() const {
			return _Cap.used;
		}

	BUFFHEADER BUFFINSTANCE BUFFNAMESPACE
		clone() const {
			return BUFFINSTANCE(static_cast<BUFFINSTANCE const&>(*this));
		}

	BUFFHEADER BUFFINSTANCE BUFFNAMESPACE
		reserve(dimension const& new_dim) const
	{
		if (is_greater(new_dim, _Cap.max)) {
			BUFFINSTANCE new_buf(new_dim);
			copy_matrix(_Ptr, _Cap.used, new_buf._Ptr);
			return new_buf;
		}
		if (is_greater(new_dim, _Cap.used)) {
			BUFFINSTANCE new_buf(clone());
			new_buf._Cap.used = new_dim;
			return new_buf;
		}
		return clone();
	}

	BUFFHEADER BUFFINSTANCE BUFFNAMESPACE
		reserve(
		dimension  const& new_dim,
		value_type const& init_val) const
	{
		BUFFINSTANCE new_buf(reserve(new_dim));
		// We only need to init if given new dimension
		// is greater the old one.
		if (is_greater(new_dim, _Cap.used)) {
			range
				init_cols(_Cap.used.second, new_dim.second),
				init_rows(_Cap.used.first, new_dim.first);
			init_matrix(new_buf._Ptr, range(0, _Cap.used.first), init_cols, init_val);
			init_matrix(new_buf._Ptr, init_rows, range(0, new_dim.second), init_val);
		}
		return new_buf;
	}

	BUFFHEADER BUFFINSTANCE BUFFNAMESPACE
		insert_rows(
			SizeType       position,
			SizeType	   count,
			ValType const& init_val) const
	{
		dimension new_dim = _Cap.used;
		new_dim.first += count;
		BUFFINSTANCE new_buf(new_dim);
		meta::insert_rows(
			_Ptr, _Cap.used, position, count, init_val, new_buf._Ptr);
		return new_buf;
	}

	BUFFHEADER BUFFINSTANCE BUFFNAMESPACE
		insert_cols(
			SizeType       position,
			SizeType	   count,
			ValType const& init_val) const
	{
		dimension new_dim = _Cap.used;
		new_dim.second += count;
		BUFFINSTANCE new_buf(new_dim);
		meta::insert_cols(
			_Ptr, _Cap.used, position, count, init_val, new_buf._Ptr);
		return new_buf;
	}

	BUFFHEADER BUFFINSTANCE BUFFNAMESPACE
		erase_rows(range const& rows) const
	{
		dimension new_dim = _Cap.used;
		new_dim.first -= length(rows);
		BUFFINSTANCE new_buf(new_dim);
		meta::erase_rows(_Ptr, _Cap.used, rows, new_buf._Ptr);
		return new_buf;
	}

	BUFFHEADER BUFFINSTANCE BUFFNAMESPACE
		erase_cols(range const& cols) const
	{
		dimension new_dim = _Cap.used;
		new_dim.second -= length(cols);
		BUFFINSTANCE new_buf(new_dim);
		if (new_dim.second) {
			meta::erase_cols(_Ptr, _Cap.used, cols, new_buf._Ptr);
		}
		return new_buf;
	}

}

#endif//SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_BUFFER_H_