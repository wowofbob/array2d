#ifndef SFEDUMMCS_EXCEPT_SAFE_ARRAY2D_IMPL_H_
#define SFEDUMMCS_EXCEPT_SAFE_ARRAY2D_IMPL_H_

#include "buffer.h"
#include <utility>

#include "reserve_politics.h"

namespace meta {

	typedef unsigned long ulong;

	template<
		typename ValType,
		typename SizeType = size_t,
		typename ReservePolitics = double_size>
	struct impl {
		
		typedef buffer<ValType, SizeType, ReservePolitics> buffer_type;
		typedef ValType value_type;

		static ulong const unshareable = -1;

		buffer_type buf;
		ulong	    refs;

		impl() :
			buf(), refs(0) {}

		impl(dimension const& dim) :
			buf(dim), refs(0) {}
		
		impl
			(dimension  const& dim,
			value_type const& init_val) :
			buf(dim, init_val), refs(0) {}

		impl(impl const& that) :
			buf(that.buf), refs(0) {}

		impl(impl&& that) :
			// refs = 0 because this impl couldn't
			// take every object which refers to that
			// impl. It only could take it's buffer.
			buf(std::move(that.buf)), refs(0)
		{
			// Nothing should do with that after
			// moving it's buffer to this imp
			// because all should be done is
			// already done by buffer itself.
			// So, that will has nullptr instead of
			// its _Ptr and zero capacity.
		}

		impl& operator= (impl const& that) {
			buf = that.buf; // No moving. Assignment operation on buffer
							// could be done both for copy and move ways.
			// Nothinf will do with that.refs
			// because we couldn't know who
			// refers to it. So, leave it.
			return *this;
		}

		impl& operator= (impl&& that) {
			buf = std::move(that.buf);
			return *this;
		}

		bool is_unshareable() const {
			return !refs || refs == unshareable;
		}

		bool is_shareable() const {
			return refs != unshareable;
		}

		impl clone() const {
			return impl(static_cast<impl const&>(*this));
		}

		void mark_unshareable() {
			refs = unshareable;
		}

		void mark_shareable() {
			refs = 0;
		}
	};

#define IMPLHEADER \
	template<\
		typename ValType,\
		typename SizeType = size_t,\
		typename ReservePolitics = double_size>

#define IMPLINSTANCE  impl<ValType, SizeType, ReservePolitics>
#define IMPLNAMESPACE IMPLINSTANCE::

	IMPLHEADER
	void release(IMPLINSTANCE*& that) {
		if (that) {
			if (that->is_unshareable()) {
				delete that;
			}
			else {
				-- that->refs;
			}
			that = nullptr;
		}
	}

	IMPLHEADER
	void capture(
		IMPLINSTANCE* that,
		IMPLINSTANCE*& to)
	{
		if (that->is_shareable()) {
			++ that->refs;
			to = that;
		}
		else {
			to = new IMPLINSTANCE(*that);
		}
	}

	IMPLHEADER
	void get_to_modify(IMPLINSTANCE*& that)
	{
		if (that->is_shareable() && that->refs) {
			IMPLINSTANCE*
				copy = new IMPLINSTANCE(*that);
			release(that);
			that = copy;
		}
	}

}

#endif//SFEDUMMCS_EXCEPT_SAFE_ARRAY2D_IMPL_H_