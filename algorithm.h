#ifndef SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_ALGORITHM_H_
#define SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_ALGORITHM_H_

#include <algorithm>
#include <iterator>

namespace meta {

	// Inserts one range into the other
	// and outputs the result into  the
	// `output`.
	template<
		typename RngIt, // range where to insert
		typename InsIt, // range which to insert
		typename OutIt>
	OutIt insert(
		RngIt rng_fst, RngIt rng_lst, RngIt position,
		InsIt ins_fst, InsIt ins_lst, OutIt output)
	{
		OutIt
			ins_beg = std::copy(rng_fst, position, output),
			ins_end = std::copy(ins_fst, ins_lst, ins_beg);
		return std::copy(position, rng_lst, ins_end);
	}

	// Do the same as the previous function but
	// instead of taking the range between
	// iterators takes the range of equal values
	// to insert
	template<
		typename FwdIt,
		typename Diff,
		typename ValT,  
		typename OutIt>
	OutIt insert_n(
		FwdIt fst, FwdIt lst, FwdIt pos,
		Diff count,
		ValT const& val,
		OutIt output)
	{
		OutIt
			ins_beg = std::copy(fst, pos, output),
			ins_end = std::fill_n(ins_beg, count, val);
		return std::copy(pos, lst, ins_end);
	}

	// Overload of previous function which
	// takes a position of where to insert
	// the range as the offset from the first
	// element in target range.
	template<
		typename FwdIt,
		typename Diff,
		typename ValT,
		typename OutIt>
	OutIt insert_n(
		FwdIt fst,    FwdIt lst,
		Diff  offset, Diff  count,
		ValT const& val,
		OutIt output)
	{
		FwdIt pos = fst;
		std::advance(pos, offset);
		return
			insert_n(fst, lst, pos, count, val, output);
	}

	// Takes two ranges and glue them into the one
	// writing the result into the `output` iterator.
	template<
		typename LftIt,
		typename RhtIt,
		typename OutIt>
	OutIt concat(
		LftIt lft_fst, LftIt lft_lst,
		RhtIt rht_fst, RhtIt rht_lst,
		OutIt output)
	{
		OutIt
			lft_end = std::copy(lft_fst, lft_lst, output),
			rht_end = std::copy(rht_fst, rht_lst, lft_end);
		return rht_end;
	}

	// Do the same as the previous one but
	// concatenates one range between iterators
	// with range of equal values,  where
	// first range is on left side, and the
	// second one is on right.
	template<
		typename FwdIt,
		typename Diff,
		typename ValT,
		typename OutIt>
	OutIt right_concat(
		FwdIt fst, FwdIt lst,
		Diff count,
		ValT const& val,
		OutIt output)
	{
		OutIt
			end = std::copy(fst, lst, output);
		return std::fill_n(end, count, val);
	}

	// Do the same as the previous one
	// but concatenates range of values
	// to the left side of the given
	// range.
	template<
		typename FwdIt,
		typename Diff,
		typename ValT,
		typename OutIt>
	OutIt left_concat(
		FwdIt fst, FwdIt lst,
		Diff count,
		ValT const& val,
		OutIt output)
	{
		OutIt
			end = std::fill_n(output, count, val);
		return std::copy(fst, lst, end);
	}
}

#endif//SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_ALGORITHM_H_