#ifndef SFEDUMMCS_EXCEPT_SAFE_ARRAY2D_ARRAY2D_H_
#define SFEDUMMCS_EXCEPT_SAFE_ARRAY2D_ARRAY2D_H_

#include "impl.h"
#include "reserve_politics.h"
#include "type_traits.h"
#include "utility.h"

#include <algorithm>
#include <iterator>

namespace meta {
	
	/*
		TODO:
			implement bidirectional iterator for array2d
	*/

	template<
		typename ValType,
		typename SizeType = size_t,
		typename ReservePolitics = double_size>
	class array2d : basic_traits<ValType, SizeType> {

	public:

		typedef ReservePolitics						            reserve_politics;
		typedef impl<value_type, size_type, reserve_politics>   impl_type;
		typedef buffer<value_type, size_type, reserve_politics> buffer_type;

		array2d();
		array2d(dimension const& dim);
		array2d(
			dimension  const& dim,
			value_type const& init_val);
		array2d(array2d const& that);
		array2d(array2d&& that);
		~array2d();

		array2d& operator= (array2d const& that);
		array2d& operator= (array2d&& that);

		inline size_type rows() const;
		inline size_type cols() const;

		inline
		reference operator()(
			size_type row,
			size_type col);
		
		inline
		const_reference operator()(
			size_type row,
			size_type col) const;

		void insert_rows(
			size_type pos,
			size_type count,
			const_reference init_val);

		void insert_cols(
			size_type pos,
			size_type count,
			const_reference init_val);

		void erase_rows(range const& rows_to_erase);
		void erase_cols(range const& cols_to_erase);

		void swap(array2d& that);

		class basic_const_iterator :
			public std::iterator<
				std::bidirectional_iterator_tag, value_type, size_type>
		{
		protected:

			basic_const_iterator(
				impl_type* arr_impl,
				size_type row,
				size_type col) :
				_Impl(arr_impl), _Row(row), _Col(col) {}

			size_type  _Row, _Col;
			impl_type* _Impl;

			basic_const_iterator() : _Row(0), _Col(0), _Impl(nullptr) {}

			basic_const_iterator(basic_const_iterator const& that) :
				_Row(0), _Col(0), _Impl(that._Impl) {}

		public:

			basic_const_iterator&
				operator= (basic_const_iterator const& that) {
					_Row = that._Row;
					_Col = that._Col;
					_Impl = that._Impl;
					return *this;
				}

			bool operator== (basic_const_iterator const& that) const {
				return
					_Impl == that._Impl &&
					_Row == that._Row &&
					_Col == that._Col;
			}

			bool operator!= (basic_const_iterator const& that) const {
				return !operator==(that);
			}

			value_type const& operator*() const {
				return _Impl->buf.get()[_Row][_Col];
			}

			pointer operator->() const {
				return &(*(*this));
			}

			virtual ~basic_const_iterator() {}
		};


#define DERIVED_FROM_BASIC_ITERATOR_DEFCOPY(DERIVED, BASIC)\
	DERIVED() : BASIC() {}\
	\
	DERIVED(DERIVED const& that) : BASIC(that) {}

#define DERIVED_FROM_BASIC_ITERATOR_ARGS(DERIVED, BASIC)\
	DERIVED(\
	impl_type* arr_impl, \
	size_type row, \
	size_type col) : \
	BASIC(arr_impl, row, col) {}

#define DERIVED_FROM_BASIC_ITERATOR(DERIVED, BASIC)\
	DERIVED_FROM_BASIC_ITERATOR_DEFCOPY(DERIVED, BASIC); \
	DERIVED_FROM_BASIC_ITERATOR_ARGS(DERIVED, BASIC);


		class basic_mutable_iterator : public basic_const_iterator {
		
		protected:
		
			DERIVED_FROM_BASIC_ITERATOR(
				basic_mutable_iterator,
				basic_const_iterator);
		
		public:
			value_type& operator*() {
				return _Impl->buf.get()[_Row][_Col];
			}

			pointer operator->() {
				return &(*(*this));
			}

			virtual ~basic_mutable_iterator() {}
		};

#define ITERATOR_TRAVERSE(CLASS, VAR) \
	CLASS& operator++() {\
		++VAR;\
		return *this;\
	}\
	\
	CLASS operator++(int) {\
		CLASS old = *this;\
		++VAR;\
		return old;\
	}\
	\
	CLASS& operator--() {\
		--VAR;\
		return *this;\
	}\
	\
	CLASS operator--(int) {\
		CLASS old = *this;\
		--VAR;\
		return old;\
	}

		template<typename ItrType>
		class row_traversible : public ItrType {
		protected:
			DERIVED_FROM_BASIC_ITERATOR(
				row_traversible, ItrType);
		public:
			ITERATOR_TRAVERSE(row_traversible, _Col);
			virtual ~row_traversible() {}
		};

		template<typename ItrType>
		class column_traversible : public ItrType {
		protected:
			DERIVED_FROM_BASIC_ITERATOR(
				column_traversible, ItrType);
		public:
			ITERATOR_TRAVERSE(column_traversible, _Row);
			virtual ~column_traversible() {}
		};

		class const_column_iterator;

		class const_row_iterator :
			public row_traversible<basic_const_iterator> {
		public:

			friend class array2d;
			friend class const_column_iterator;

			// Why should explicit constructor be here?

			DERIVED_FROM_BASIC_ITERATOR_DEFCOPY(
				const_row_iterator,
				row_traversible<basic_const_iterator>);
		
			const_column_iterator get_itr() const;

		private:

			DERIVED_FROM_BASIC_ITERATOR_ARGS(
				const_row_iterator,
				row_traversible<basic_const_iterator>);
		
		};

		class column_iterator;

		class row_iterator :
			public row_traversible<basic_mutable_iterator> {
		public:

			friend class array2d;
			friend class column_iterator;

			DERIVED_FROM_BASIC_ITERATOR_DEFCOPY(
				row_iterator,
				row_traversible<basic_mutable_iterator>);
			
			//const_column_iterator get_citr() const;
			column_iterator get_itr() const;

		private:

			DERIVED_FROM_BASIC_ITERATOR_ARGS(
				row_iterator,
				row_traversible<basic_mutable_iterator>);
		};

		
		class const_column_iterator :
			public column_traversible<basic_const_iterator>  {
		public:

			friend class array2d;
			friend class const_row_iterator;

			DERIVED_FROM_BASIC_ITERATOR_DEFCOPY(
				const_column_iterator,
				column_traversible<basic_const_iterator>);

			const_row_iterator get_itr() const;

		private:
			DERIVED_FROM_BASIC_ITERATOR_ARGS(
				const_column_iterator,
				column_traversible<basic_const_iterator>);
		};


		class column_iterator :
			public column_traversible<basic_mutable_iterator>  {
		public:

			friend class array2d;
			friend class row_iterator;

			DERIVED_FROM_BASIC_ITERATOR_DEFCOPY(
				column_iterator,
				column_traversible<basic_mutable_iterator>);

			row_iterator get_itr() const;

		private:
			DERIVED_FROM_BASIC_ITERATOR_ARGS(
				column_iterator,
				column_traversible<basic_mutable_iterator>);
		};

		const_row_iterator row_cbegin(size_type row) {
			return const_row_iterator();
		}

		row_iterator row_begin(size_type row) {
			get_to_modify(_Impl);
			_Impl->mark_unshareable();
			return row_iterator(_Impl, row, 0);
		}

		const_column_iterator column_cbegin(size_type col) {
			return const_column_iterator(_Impl, 0, col);
		}

		column_iterator column_begin(size_type col) {
			get_to_modify(_Impl);
			_Impl->mark_unshareable();
			return column_iterator(_Impl, 0, col);
		}

	private:

		void change_buffer(buffer_type&& buf);

		impl_type* _Impl;
	};

#define ARRHEADER \
	template<\
	typename ValType, \
	typename SizeType = size_t, \
	typename ReservePolitics = double_size>

#define ARRINSTANCE  array2d<ValType, SizeType, ReservePolitics>
#define ARRNAMESPACE ARRINSTANCE::

	ARRHEADER ARRNAMESPACE
		array2d() :
		_Impl(nullptr)
	{
		_Impl = new impl_type();
	}

	ARRHEADER ARRNAMESPACE
		array2d(dimension const& dim) :
		_Impl(nullptr)
	{
		_Impl = new impl_type(dim);	
	}

	ARRHEADER ARRNAMESPACE
		array2d(
		dimension  const& dim,
		value_type const& init_val) :
		_Impl(nullptr)
	{
		_Impl = new impl_type(dim, init_val);
	}

	ARRHEADER ARRNAMESPACE
		array2d(ARRINSTANCE const& that) :
		_Impl(nullptr)
	{
		capture(that._Impl, _Impl);
	}

	ARRHEADER ARRNAMESPACE
		array2d(ARRINSTANCE&& that) :
		_Impl(that._Impl)
	{
		release(that._Impl);
	}

	ARRHEADER ARRNAMESPACE
		~array2d()
	{
		release(_Impl);
	}

	ARRHEADER ARRINSTANCE& ARRNAMESPACE
		operator= (ARRINSTANCE const& that)
	{
		release(_Impl);
		capture(that._Impl, _Impl);
		return *this;
	}

	ARRHEADER ARRINSTANCE& ARRNAMESPACE
		operator= (ARRINSTANCE&& that)
	{
		release(_Impl);
		_Impl = that._Impl;
		that._Impl = nullptr;
		return *this;
	}

	ARRHEADER SizeType ARRNAMESPACE
		rows() const {
			return _Impl->buf.dim().first;
		}

	ARRHEADER SizeType ARRNAMESPACE
		cols() const {
			return _Impl->buf.dim().second;
		}

	ARRHEADER ValType& ARRNAMESPACE
		operator()(SizeType row, SizeType col)
	{
		get_to_modify(_Impl);
		_Impl->mark_unshareable();
		return _Impl->buf.get()[row][col];
	}

	ARRHEADER ValType const& ARRNAMESPACE
		operator()(SizeType row, SizeType col) const
	{
		return _Impl->buf.get()[row][col];
	}

	ARRHEADER void ARRNAMESPACE
		change_buffer(buffer<ValType, SizeType, ReservePolitics>&& new_buf)
	{
		if (_Impl->is_shareable()) {
			release(_Impl);
			_Impl = new impl_type();
		}
		_Impl->buf = new_buf;
		_Impl->mark_shareable();
	}

	ARRHEADER void ARRNAMESPACE
		insert_rows(
			size_type pos,
			size_type count,
			const_reference init_val)
		{
			buffer_type
				new_buf = _Impl->buf.insert_rows(pos, count, init_val);
			change_buffer(std::move(new_buf));
		}

	ARRHEADER void ARRNAMESPACE
		insert_cols(
			size_type pos,
			size_type count,
			const_reference init_val)
		{
			buffer_type
				new_buf = _Impl->buf.insert_cols(pos, count, init_val);
			change_buffer(std::move(new_buf));
		}

	ARRHEADER void ARRNAMESPACE
		erase_rows(range const& rows_to_erase)
	{
		buffer_type
			new_buf = _Impl->buf.erase_rows(rows_to_erase);
		change_buffer(std::move(new_buf));
	}

	ARRHEADER void ARRNAMESPACE
		erase_cols(range const& cols_to_erase)
	{
		buffer_type
			new_buf = _Impl->buf.erase_cols(cols_to_erase);
		change_buffer(std::move(new_buf));
	}

	ARRHEADER void ARRNAMESPACE
		swap(ARRINSTANCE& that)
	{
		std::swap(_Impl, that._Impl);
	}
}

#endif//SFEDUMMCS_EXCEPT_SAFE_ARRAY2D_ARRAY2D_H_