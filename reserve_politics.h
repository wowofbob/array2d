#ifndef SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_RESERVE_POLITCS_H_
#define SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_RESERVE_POLITCS_H_

namespace meta {

	struct double_size {
		inline size_t
		operator()(size_t size) const
		{
			return size * 2;
		}
	};

}

#endif//SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_RESERVE_POLITCS_H_