#ifndef SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_UTILITY_H_
#define SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_UTILITY_H_

#include <utility>

namespace meta {
	
	typedef std::pair<size_t, size_t> range;

	inline
	size_t length(range const& r) {
		return r.second - r.first;
	}

}

#endif//SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_UTILITY_H_