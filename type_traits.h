#ifndef SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_TYPE_TRAITS_H_
#define SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_TYPE_TRAITS_H_

#include <type_traits>

namespace meta {

	template<
		typename ValType>
	struct value_traits {
		typedef ValType		   value_type;
		typedef ValType*	   pointer;
		typedef ValType&	   reference;
		typedef ValType const& const_reference;
		virtual ~value_traits() {}
	};

	template<
		typename SizeType>
	struct size_traits {
		typedef SizeType size_type;
		virtual ~size_traits() {}
	};

	template<
		typename ValType,
		typename SizeType>
	struct basic_traits {
		typedef SizeType	   size_type;
		typedef ValType		   value_type;
		typedef ValType*	   pointer;
		typedef ValType&	   reference;
		typedef ValType const& const_reference;
		virtual ~basic_traits() {}
	};


}

#endif//SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_TYPE_TRAITS_H_