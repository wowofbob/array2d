#ifndef SFEDUMMCS_META_EXCEPT_SAFE_MATRIX_H_
#define SFEDUMMCS_META_EXCEPT_SAFE_MATRIX_H_

#include "allocator.h"

#include "algorithm"
#include "utility.h"

#include <functional>

namespace meta {

	using namespace std::placeholders;

	/*
		Contains functions to operate with memoty in form of matrix.
	*/

	typedef std::pair<size_t, size_t> dimension;

	template<typename ValType>
	ValType** create_matrix(dimension const& dim) {
		
		ValType** mat = allocate_array<ValType*, size_t>(dim.first);

		if (dim.second) {
			array_allocator<ValType> alloc(dim.second);
			std::generate_n(mat, dim.first, alloc);
		}
		else {
			std::fill_n(mat, dim.first, nullptr);
		}
		
		return mat;

	}

	template<typename ValType>
	void delete_matrix(ValType** mat, dimension const& dim) {
		ValType **fst = mat, **lst = mat + dim.first;
		std::for_each(fst, lst, delete_array<ValType>);
		delete_array(mat);
	}

	template<typename ValType>
	std::function<ValType* (ValType*)>
		init_row(size_t count, ValType const& init_val) {
			return std::bind(std::fill_n<ValType*, size_t, ValType>, _1, count, init_val);
		}

	template<typename ValType>
	std::function<void (ValType*)>
		init_row(range const& cols, ValType const& init_val) {
			return
				[&cols, &init_val](ValType* row) {
					ValType
						*fst = row + cols.first,
						*lst = row + cols.second;
					std::fill(fst, lst, init_val);
				};
		}

	template<typename ValType>
	void init_matrix(ValType** mat,
		range const& rows,
		range const& cols,
		ValType const& init_val)
	{
		auto init = init_row(cols, init_val);
		ValType
			**fst = mat + rows.first,
			**lst = mat + rows.second;
		/*
			I don't use std::transform below because
			init_row function returns the pointer to
			the last initialized element in array
			(such as std::fill_n).
			std::for_each do not transform sequence
			using assignment operator, so, I can use
			init_row here and still init sequence,
			because it's operates with pointers.
		*/
		std::for_each(fst, lst, init);
	}

	template<typename ValType>
	ValType** copy_matrix(
		ValType** mat, dimension const& dim,
		size_t offset_mat,
		size_t offset_out,
		ValType** output)
	{
		ValType
			**fst = mat,
			**lst = mat + dim.first,
			**res = output;
		std::for_each(fst, lst,
			[&](ValType* row) {
			ValType
				*fst = row      + offset_mat,
				*out = *(res++) + offset_out;
				std::copy_n(fst, dim.second, out);
			});
		return res;
	}

	template<typename ValType>
	ValType** copy_matrix(
		ValType** mat, dimension const& dim, ValType** output)
	{
		return copy_matrix(mat, dim, 0, 0, output);
	}


	template<typename ValType>
	ValType** insert_rows(
		ValType** mat, dimension const& dim,
		size_t pos, size_t count, ValType const& init_val,
		ValType** output)
	{
		dimension
			dim_head(pos, dim.second),
			dim_tail(dim.first - pos, dim.second);
		auto init = init_row(dim.second, init_val);
		ValType
			**ins_beg = copy_matrix(mat, dim_head, output),
			**ins_end = ins_beg + count;
		std::for_each(ins_beg, ins_end, init);
		return
			copy_matrix(mat + pos, dim_tail, ins_end);
	}

	template<typename ValType>
	void insert_cols(
		ValType** mat, dimension const& dim,
		size_t pos, size_t count, ValType const& init_val,
		ValType** output)
	{
		ValType
			**fst = mat,
			**lst = mat + dim.first,
			**res = output;
		std::for_each(fst, lst,
			[&](ValType* row) {
				ValType *fst = row, *lst = fst + dim.second;
				insert_n(fst, lst, pos, count, init_val, *(res++));
			});
	}

	template<typename ValType>
	ValType** erase_rows(
		ValType** mat, dimension const& dim,
		range const& rows,
		ValType** output)
	{
		dimension
			head(rows.first, dim.second),
			tail(dim.first - rows.second, dim.second);
		ValType
			**erase_beg = copy_matrix(mat, head, output);
		return
			copy_matrix(mat + rows.second, tail, erase_beg);
	}

	template<typename ValType>
	void erase_cols(
		ValType** mat, dimension const& dim,
		range const& cols,
		ValType** output)
	{
		dimension
			left(dim.first, cols.first),
			right(dim.first, dim.second - cols.second);
		copy_matrix(mat, left, output);
		copy_matrix(mat, right, cols.second, cols.first, output);
	}

}

#endif//SFEDUMMCS_META_EXCEPT_SAFE_MATRIX_H_