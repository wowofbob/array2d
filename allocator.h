#ifndef SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_ALLOCATOR_H_
#define SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_ALLOCATOR_H_

#include "type_traits.h"

namespace meta {

	// Simply allocates an arrays of given size at construction time.
	
	template<
		typename ValType,
		typename SizeType = size_t>
	ValType* allocate_array(SizeType size) {
		if (!size) {
			return nullptr;
		}
		else {
			return new ValType[size];
		}
	}

	template<
		typename ValType,
		typename SizeType = size_t>
	class array_allocator :
		value_traits<ValType>,
		size_traits<SizeType> {
	public:
		
		array_allocator(
			size_type size = size_type()) : _Size(size) {}

		array_allocator(
			array_allocator const& that)  : _Size(that._Size) {};
		
		array_allocator& operator= (array_allocator const& that) {
			_Size = that._Size;
			return *this;
		}

		inline
		pointer operator()() const {
			return allocate_array<value_type, size_type>(_Size);
		}

	private:

		size_type _Size;

	};

	template<typename ValType>
	void delete_array(ValType* arr) {
		delete[] arr;
		arr = nullptr;
	};

}

#endif//SFEDUMMCS_META_EXCEPT_SAFE_ARRAY2D_ALLOCATOR_H_