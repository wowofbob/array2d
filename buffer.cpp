#include "stdafx.h"
#include "buffer.h"

namespace meta {

	capacity::capacity() :
		used(0, 0), max(0, 0) {}

	capacity::capacity(
		dimension const& u,
		dimension const& m) :
		used(u), max(m) {}

	void capacity::discard() {
		used = dimension(0, 0);
		max  = dimension(0, 0);
	}

}