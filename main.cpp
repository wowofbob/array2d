// array2d.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "algorithm.h"
#include "allocator.h"
#include "array2d.h"
#include "buffer.h"
#include "impl.h"
#include "matrix.h"

#include <assert.h>
#include <memory>
#include <iostream>
using namespace meta;

struct print {
	template<typename Val>
	void operator()(Val const& val) const {
		std::cout << val << ' ';
	}
};

class print_array {
public:

	template<typename ValT>
	void operator()(ValT* arr) const {
		std::for_each(arr, end(arr), print());
		std::cout << std::endl;
	}

	print_array(size_t size) : _Size(size) {}

private:

	template<typename ValT>
	ValT* end(ValT* arr) const {
		return arr + _Size;
	}

	size_t _Size;
};

template<typename ValT>
void print_matrix(ValT** mat, size_t rows, size_t cols) {
	print_array print(cols);
	std::for_each(mat, mat + rows, print);
}

template<typename ValT>
void print_matrix(ValT** mat, dimension const& dim) {
	print_matrix(mat, dim.first, dim.second);
}

int f(int x, int y) {
	return x - y;
}

template<typename ValType, typename SizeType = size_t>
void print_buffer(buffer<ValType, SizeType> const& buf) {
	print_matrix(buf.get(), buf.dim());
}

template<
	typename ValType,
	typename SizeType,
	typename ReservePolitics>
void print_array2d(array2d<ValType, SizeType, ReservePolitics> const& arr)
{
	for (size_t i = 0; i < arr.rows(); ++i) {
		for (size_t j = 0; j < arr.cols(); ++j) {
			std::cout << arr(i, j) << ' ';
		}
		std::cout << std::endl;
	}
}

struct s {
	int a, b;
	double d;
};

struct eq {
	int b;
	eq(int a) : b(a) {}
	bool operator==(eq const& that) const {
		return b == that.b;
	}
};

struct eq_der : eq {
	eq_der(int a) : eq(a) {}
};

int _tmain(int argc, _TCHAR* argv[])
{
	eq e(1), k(1);
	
	assert(e == k);

	eq_der ed(1), kd(1);
	assert(ed == kd);

	array2d<int> arr(dimension(9, 9), 11),
		arr1 = arr;

	assert(arr.rows() == 9);
	assert(arr1.rows() == 9);

	int n = arr1(1, 1);
	assert(arr1.rows());
	assert(n == 11);
	
	//print_array2d(arr1);

	arr1.insert_rows(1, 3, 100);
	arr1.insert_cols(3, 4, 17);
	arr1.erase_rows(range(0, 1));
	arr1.erase_cols(range(1, 2));
	//print_array2d(arr1);
	arr1.swap(arr);

	//array2d<int>::row_traversible<array2d<int>::basic_mutable_iterator> itr, itr2(itr);
	array2d<int>::const_row_iterator itr = arr.row_cbegin(0);
	std::cout << *itr << std::endl;
	++itr; ++itr;
	std::cout << *itr << std::endl;

	array2d<int>::const_column_iterator itrcol = arr.column_cbegin(0);
	//print_array2d(arr);
//
	//impl<int> im;
	//im.buf = im.buf.reserve(dimension(3, 3), 2);

	//print_buffer(im.buf);

	//impl<int> im1(im);
	//assert(im1.refs == im.refs);

	//impl<int> im2;
	//im2 = im1;
	//assert(im1.buf.get() != nullptr);

	//assert(im2.is_unshareable());
	//impl<int> *i = new impl<int>();
	//assert(i->is_unshareable());
	////release(i);

	//impl<int>* im3 = nullptr;
	//capture(&im2, im3);
	////delete i;
	//assert(im3->refs == 1);
	//get_to_modify(im3);

	return 0;
}

